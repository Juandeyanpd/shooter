using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    #region Variables
    public PlayerController playerController;
    public Gun[] guns;
    public Gun actualGun;
    public int indexGun = 0;
    public int maxGuns = 3;

    [Header("Recovery")]
    float lastShootTime = 0;
    Vector3 currentRotation;
    Vector3 targetRotation;
    public float snappines;
    public float returnSpeed;

    public GameObject preBulletHole;

    [Header("Reload")]
    float lastReaload = 0;
    bool realoading = false;

    [Header("ChangeGun")]
    float lastChangeTime = 0;
    bool isChangin = false;
    public float changeTime;

    [Header("FloorGun")]
    public float maxDistance;
    public LayerMask gun;
    #endregion

    #region Unity Functions
    private void Update()
    {
        if (Input.GetButtonDown("FloorGun") && Physics.Raycast(playerController.cam.transform.position, playerController.cam.transform.forward, out RaycastHit hit, maxDistance, gun))
        {
            
            Transform floorGun = hit.transform;

            if (hit.transform != null)
            {
                FloorGun(floorGun, indexGun);
            }
        }

        if (actualGun != null)
        {

            if (lastShootTime <= 0 && !realoading)
            {
                if (!actualGun.data.automatic)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
            }
            if (Input.GetButtonDown("Reload") && !realoading)
            {
                if (actualGun.data.actualAmmo < actualGun.data.maxAmmoCount)
                {
                    Sound.Instance.PlaySoundReload(actualGun.transform.position, actualGun.data.id);
                    lastReaload = 0;
                    realoading = true;
                }
            }

            if (realoading)
            {
                lastReaload += Time.deltaTime;
                Debug.Log(lastReaload);
                if (lastReaload >= actualGun.data.reloadTime)
                {
                    realoading = false;
                    Reload();
                }
            }
        }

        if (lastShootTime >= 0)
        {
            lastShootTime -= Time.deltaTime;
        }

        if (isChangin)
        {
            lastChangeTime += Time.deltaTime;
            if (lastChangeTime >= changeTime)
            {
                isChangin = false;
                ChangeGun(indexGun);
            }
        }

        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, returnSpeed * Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targetRotation, snappines * Time.deltaTime);
        playerController.recoil.localRotation = Quaternion.Euler(currentRotation);

        if (Input.GetButtonDown("Key1") && !realoading)
        {
            if (indexGun != 0)
            {
                indexGun = 0;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChangin = true;
            }
        }
        if (Input.GetButtonDown("Key2") && !realoading)
        {
            if (indexGun != 1)
            {
                indexGun = 1;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChangin = true;
            }
        }
        if (Input.GetButtonDown("Key3") && !realoading)
        {
            if (indexGun != 2)
            {
                indexGun = 2;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChangin = true;
            }
        }

        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color =  Color.green;
        Gizmos.DrawRay(playerController.cam.transform.position,  playerController.cam.transform.forward * maxDistance);
    }
    #endregion
    #region Custom Functions
    private void Shoot()
    {
        if (Physics.Raycast(playerController.cam.transform.position, playerController.cam.transform.forward, out RaycastHit hit, actualGun.data.range))
        {
            if (hit.transform != null)
            {
                Debug.Log($"We Shootin at: {hit.transform.name}");
                Instantiate(preBulletHole, hit.point + hit.normal * 0.0001f, Quaternion.LookRotation(hit.normal, Vector3.up));
            }
        }
        actualGun.data.actualAmmo--;
        lastShootTime = actualGun.data.fireRate;
        Addrecoil();
        
        Sound.Instance.PlaySoundShot( actualGun.transform.position, actualGun.data.id);
    }

    void Addrecoil()
    {
        targetRotation += new Vector3(-actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y), 0f);
    }

    void Reload()
    {
        actualGun.data.actualAmmo = actualGun.data.maxAmmoCount;
    }

    void ChangeGun(int index)
    {
        //Este lo hice yo
        /*while (indexGun < guns.Length)
        {
            if (actualGun != null)
            {
                actualGun.gameObject.SetActive(false);
                guns[index] = actualGun;
                guns[index].gameObject.SetActive(true);
            }
            else
            {
                indexGun++;
            }
        }*/

        if (guns[index] != null)
        {
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);
        }
    }

    void FloorGun(Transform floorGun, int index)
    {
        if (actualGun != null)
        {
            ReleaseGun(actualGun, indexGun);
        }

        Rigidbody rb = floorGun.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        floorGun.transform.parent = playerController.gunPoint;
        actualGun = floorGun.transform.gameObject.GetComponent<Gun>();
        floorGun.transform.localPosition = actualGun.data.offset;
        floorGun.transform.localRotation = Quaternion.identity;
        guns[index] = actualGun;

    }

    void ReleaseGun(Gun floorGun, int index)
    {
        Rigidbody rb = floorGun.GetComponent<Rigidbody>();
        actualGun.transform.parent = null;
        guns[index] = null;
        actualGun = null;
        rb.isKinematic = false;
        rb.AddForce(floorGun.transform.up * 2, ForceMode.Impulse);
        rb.AddTorque(floorGun.transform.right * 2, ForceMode.Impulse);
    }
    #endregion

}
