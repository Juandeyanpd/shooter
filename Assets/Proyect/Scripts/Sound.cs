using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public static Sound Instance;
    public AudioClip[] shot;
    public AudioClip[] reload;
    public AudioSource aSource;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void PlaySoundShot(Vector3 soundPoint, int id)
    {
        transform.position = soundPoint;
        aSource.PlayOneShot(shot[id]);
    }

    public void PlaySoundReload(Vector3 soundPoint, int id)
    {
        transform.position = soundPoint;
        aSource.PlayOneShot(reload[id]);
    }
}
