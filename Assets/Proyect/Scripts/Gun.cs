using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Gun : MonoBehaviour
{
    #region Variables
    public GunData data;
    public VisualEffect effect;
    public GameObject shot;

    #endregion

    #region Unity Functions
    private void Awake()
    {
        data.actualAmmo = data.maxAmmoCount;
    }
    #endregion
}
