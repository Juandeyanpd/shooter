using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;

public class RoomButton : MonoBehaviour
{
    [SerializeField] TMP_Text button_Text;
    public RoomInfo info;

    #region Custom Functions
    public void SetButtonDetails(RoomInfo rInfo)
    {
        info = rInfo;
        button_Text.text = info.Name;
    }

    public void JoinRoom()
    {
        Launcher.Instance.JoiningRoom(info);
    }
    #endregion
}
