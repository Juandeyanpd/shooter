using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Variables
    public Camera cam;
    public Transform recoil;
    public Transform gunPoint;

    [SerializeField] private CharacterController controller;
    [SerializeField] private Transform pointOfView;
    [SerializeField] private float walkSpeed = 5f;
    [SerializeField] private float runSpeed = 8f;
    [SerializeField] private float jumpForce = 12f;
    [SerializeField] private float gravityMod = 2.5f;


    private float actualSpeed;
    private float horizontalRotationStore;
    private float verticalRotationStore;

    private Vector2 mouseInput;
    private Vector3 direction;
    private Vector3 movement;

    [Header("Ground Detection")]
    [SerializeField] private bool isGround;
    [SerializeField] private float radio;
    [SerializeField] private float distance;
    [SerializeField] private Vector3 offset;
    [SerializeField] private LayerMask lm;

    public GameManager gameManager;
    
    #endregion

    #region Unity Functions
    private void Start()
    {
        controller = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
    }
    private void Update()
    {
        Rotation();
        Movement();
    }

    private void LateUpdate()
    {
        cam.transform.position = recoil.transform.position; 
        cam.transform.rotation = recoil.transform.rotation;

        gunPoint.transform.position = recoil.transform.position;
        gunPoint.transform.rotation = recoil.transform.rotation;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offset, radio);
        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distance, lm))
        {
            Gizmos.color = Color.green;
            Vector3 endPoint = (transform.position + offset) + (Vector3.down * distance);
            Gizmos.DrawWireSphere(endPoint, radio);
            Gizmos.DrawLine(transform.position + offset, endPoint);

            Gizmos.DrawSphere(hit.point, 0.1f);
        }
        else
        {
            Gizmos.color = Color.red;
            Vector3 endPoint = (transform.position + offset) + (Vector3.down * distance);
            Gizmos.DrawWireSphere(endPoint, radio);
            Gizmos.DrawLine(transform.position + offset, endPoint);
        }
    }
    #endregion

    #region Custom Functions
    private void Rotation()
    {
        mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        horizontalRotationStore += mouseInput.x; 
        verticalRotationStore -= mouseInput.y;

        verticalRotationStore = Mathf.Clamp(verticalRotationStore, -60f, 60f);

        transform.rotation = Quaternion.Euler(0f,horizontalRotationStore,0f);
        pointOfView.transform.localRotation = Quaternion.Euler(verticalRotationStore, 0f, 0f);
    }

    private void Movement()
    {
        direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));

        float velY = movement.y;
        movement = ((transform.forward * direction.z) + (transform.right * direction.x)).normalized;
        movement.y = velY;

        if(Input.GetButton("Fire3"))
        { 
            actualSpeed = runSpeed;
        }
        else
        {
            actualSpeed = walkSpeed;
        }

        if(controller.isGrounded)
        {
            movement.y = 0;
        }

        if(Input.GetButtonDown("Jump") && isGrounded())
        {
            movement.y = jumpForce;
        }

        movement.y += Physics.gravity.y * Time.deltaTime * gravityMod;
        controller.Move(movement * (actualSpeed * Time.deltaTime)); 
    }

    private bool isGrounded()
    {
        isGround = false;

        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distance, lm))
        {
            isGround = true;
        }
        return isGround;
    }
    #endregion
}
