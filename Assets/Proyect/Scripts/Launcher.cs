using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
    #region Variables
    public static Launcher Instance;

    [SerializeField] GameObject[] screenObjects;

    [SerializeField] TMP_InputField roomNameInput;

    [SerializeField] TMP_Text infoText;
    [SerializeField] TMP_Text nameRoom;
    [SerializeField] TMP_Text errorRoom;

    [SerializeField] RoomButton roomButton;
    [SerializeField] Transform content_Scroll;
    [SerializeField] List<RoomButton> roomButtonList = new List<RoomButton>();

    #endregion

    #region Unity Fuctions
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        infoText.text = "Connecting to Network...";
        SetScreenObjects(0);
        PhotonNetwork.ConnectUsingSettings();
    }
    #endregion

    #region Custom Functions

    public void SetScreenObjects(int index)
    {
        for (int i = 0; i < screenObjects.Length; i++)
        {
            screenObjects[i].SetActive(i == index);
        }
    }

    public void CreateRoom()
    {
        if (!string.IsNullOrEmpty(roomNameInput.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 8;
            PhotonNetwork.CreateRoom(roomNameInput.text);
            infoText.text = "Creating Room...";
            SetScreenObjects(0);
        }
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        infoText.text = "Leaving Room";
        SetScreenObjects(0);
    }
    #endregion

    #region Photon
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetScreenObjects(1);
    }

    public override void OnJoinedRoom()
    {
        nameRoom.text = PhotonNetwork.CurrentRoom.Name; 
        SetScreenObjects(4);
    }



    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        errorRoom.text = "Se fallo al crear la room" + message;
        SetScreenObjects(5);
    }

    public override void OnLeftRoom()
    {
        SetScreenObjects(1);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        bool createRoom = false;

        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].RemovedFromList)
            {
                for (int j = 0; j < roomButtonList.Count; j++)
                {
                    if (roomList[i].Name == roomButtonList[j].info.Name)
                    {
                        GameObject go = roomButtonList[j].gameObject;
                        roomButtonList.Remove(go.GetComponent<RoomButton>());
                        Destroy(go);
                    }
                }
            }
        }

        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].PlayerCount != roomList[i].MaxPlayers && !roomList[i].RemovedFromList)
            {
                RoomButton newRoomButton = Instantiate(roomButton, content_Scroll);
                newRoomButton.SetButtonDetails(roomList[i]);
                roomButtonList.Add(newRoomButton);
            }
        }
    }

    public void JoiningRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoText.text = "Joining Room";
        SetScreenObjects(0);
    }
    #endregion
}
